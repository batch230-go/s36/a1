const Task = require("../models/Task.js");

// function createTaskController(req, res){ //statements } << export this funtion
module.exports.createTaskController = (req, res) => {
	// checking captured data from the request body.
	console.log(req.body);

	Task.findOne({name: req.body.name}).then(result =>
	{
		console.log(result);

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}
		else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
}


// Retrieve all task
module.exports.getAllTaskController = (req, res) => {
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}


// Deleting a task
module.exports.deleteTaskController = (req, res) => {
	console.log(req.params.taskId)

	Task.findByIdAndRemove(req.params.taskId)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}


// Update Specific Task through URL
module.exports.changeNameToArchive = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){

			console.log(err);
			return err;

		}

		// Change the status of the returned document to "complete"
		result.name = "Archived Task";

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter, we can use the "save" method to update the existing document with the changes we applied
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method  which invokes this function
		return result.save().then((updatedTask, saveErr) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr) {

				console.log(saveErr);
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return saveErr;

			// Update successful, returns the updated task object back to the client/Postman
			} else {

				// The "return" statement returns the result to the  "then" method chained to the "save" method which invokes this function
				return updatedTask;

			}
		})
	})

}


// Updating name of a task
module.exports.updateTaskNameController = (req, res) => {
	console.log(req.params.taskId);
	console.log(req.body);

	let updates = {
		name: req.body.name
	}

	Task.findByIdAndUpdate(req.params.taskId, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}


// Activity Part
// [GET -Wildcard required]
/*
	1. Create a controller function for retrieving a specific task.
	2. Create a route 
	3. Return the result back to the client/Postman.
	4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
*/
module.exports.getSpecificIdController = (request, response) => {
	Task.findById(request.params.id)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


// [PUT - Update - Wildcard required]
/*
	5. Create a controller function for changing the status of a task to "complete".
	6. Create a route
	7. Return the result back to the client/Postman.
	8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
*/
module.exports.updateTaskStatusController = (id) => {
	return Task.findById(id).then((result, err) => {
		if(err){
			console.log(err);
			return err;
		}
		result.status = "complete";
		return result.save().then((updatedStatus, saveError) => {
			if (saveError) {
				console.log(saveError);
				return saveError;
			} else {
				return updatedStatus;
			}
		})
	})
}
