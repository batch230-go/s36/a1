/*
	Preparations:
	npm init -y
	npm install express
	npm install mongoose
*/


// require the installed modules
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js");

// server
const app = express();

// port
const port = 4000;

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch230.pm3o66l.mongodb.net/S36?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log(`We're connected to the cloud database`));

// middlewares
app.use(express.json());

// Routes Grouping - organize the access for each resources.
app.use("/tasks", taskRoutes); //localhost:4000/tasks/


// port listener
app.listen(port, () => console.log(`Server is runnig at port ${port}`))