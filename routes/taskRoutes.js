const express = require("express");
// express.Router() method allows access to HTTP methods
const router = express.Router();
const taskController = require("../controllers/taskController.js");

// Create - task routes
router.post("/addTask", taskController.createTaskController);

// Get all tasks
router.get("/allTask", taskController.getAllTaskController);

// Deleting a task
router.delete("/deleteTask/:taskId", taskController.deleteTaskController);

router.put("/:id/archive", (req, res) => {
    taskController.changeNameToArchive(req.params.id)
    .then(resultFromController => res.send(resultFromController));
});

router.patch("/updateTask/:taskId", taskController.updateTaskNameController);


// Activity Part
// GET Part
router.get("/:id", taskController.getSpecificIdController);

// PUT Part
router.put("/:id/complete", (req, res) => {
    taskController.updateTaskStatusController(req.params.id)
    .then(result => res.send(result));
});

module.exports = router; // << this is a middleware
